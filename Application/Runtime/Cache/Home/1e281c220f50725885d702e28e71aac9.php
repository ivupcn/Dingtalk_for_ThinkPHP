<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>My App</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_1465179629_0734837.css">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
</head>
<body>
<!-- Panels Overlay -->
<div class="panel-overlay"></div>
<!-- Right Panel with Cover effect -->
<div class="panel panel-right panel-cover layout-dark">
    <div class="content-block-title">审批</div>
    <div class="list-block">
        <ul>
            <li><a href="list-view.html" class="item-link close-panel">
                <div class="item-content">
                    <div class="item-media"><i class="icon icon-f7"></i></div>
                    <div class="item-inner">
                        <div class="item-title">我发起的审批</div>
                    </div>
                </div>
            </a></li>
            <li><a href="forms.html" class="item-link close-panel">
                <div class="item-content">
                    <div class="item-media"><i class="icon icon-f7"></i></div>
                    <div class="item-inner">
                        <div class="item-title">已审批</div>
                    </div>
                </div>
            </a></li>
            <li><a href="list-view.html" class="item-link close-panel">
                <div class="item-content">
                    <div class="item-media"><i class="icon icon-f7"></i></div>
                    <div class="item-inner">
                        <div class="item-title">未审批</div>
                    </div>
                </div>
            </a></li>
            <li><a href="list-view.html" class="item-link close-panel">
                <div class="item-content">
                    <div class="item-media"><i class="icon icon-f7"></i></div>
                    <div class="item-inner">
                        <div class="item-title">待我审批</div>
                    </div>
                </div>
            </a></li>
            <li><a href="list-view.html" class="item-link close-panel">
                <div class="item-content">
                    <div class="item-media"><i class="icon icon-f7"></i></div>
                    <div class="item-inner">
                        <div class="item-title">我已审批</div>
                    </div>
                </div>
            </a></li>
        </ul>
    </div>
</div>
<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-left">
                <div class="center sliding" style="left: 105px; transform: translate3d(-86px, 0px, 0px);">Framework7
                </div>
                <div class="right"><a href="#" class="open-panel link icon-only"><i class="icon icon-bars"></i></a>
                </div>
            </div>
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);"><a href="index.html"
                                                                                            class="back link"><i
                        class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i><span
                        class="">返回</span></a></div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">审批</div>
                <div class="right"><a href="#" data-panel="right" class="open-panel link icon-only"><i
                        class="icon icon-bars"></i></a>
                </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div data-page="index" class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    <div class="content-block">
                        <div class="row">
                            <div class="col-50"><a href="#" class="button open-panel">未审批</a></div>
                            <div class="col-50"><a href="#" data-panel="right" class="button open-panel">已审批</a></div>
                        </div>
                    </div>
                    <div class="ks-grid">
                        <div class="row no-gutter">
                            <a href="#" class="col-33"><i class="iconfont icon-qingjia" style="color:#FF8C00"></i>请假</a>
                            <a href="#" class="col-33"><i class="iconfont icon-chuchashenqing" style="color:#B0E2FF"></i>出差</a>
                            <a href="#" class="col-33"><i class="iconfont icon-baoxiao" style="color:#FFEC8B"></i>报销</a>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
</body>
</html>