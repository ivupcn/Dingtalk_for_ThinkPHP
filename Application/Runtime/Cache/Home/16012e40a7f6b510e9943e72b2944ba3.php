<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>公告</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    
    <style>
        .tppage a{
            margin: 0 2px;

        }
    </style>

</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);">
                    
                        <a href="javascript:" onclick="history.back();" class="back link">
                            <i class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i>
                            <span class="">返回</span>
                        </a>
                    

                </div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">公告</div>
                    <div class="right">
                        
                    </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    
    <div class="list-block">

            <ul id="list">
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="item-content">
                        <div class="item-media"></div>
                        <a href="<?php echo U('Home/Notice/details',array('id'=>$vo['id']));?>" class="item-inner external" data-info="<?php echo ($vo["id"]); ?>">
                            <div class="item-title"><?php echo ($vo["title"]); ?></div>
                            <div class="item-after"><?php echo date('Y-m-d',$vo['time']);?></div>
                        </a>
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>

            </ul>
        <?php echo ($page); ?>
    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/AmazeUI/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->

    <script src="http://g.alicdn.com/ilw/ding/0.9.2/scripts/dingtalk.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.IsRead = function (a) {
                var agm = {
                    liclass: '.item-content',
                    aclass: '.item-inner',
                    item_media:'.item-media',
                };
                var o = $.extend(agm, a);
                var _this = $(this);
                var liclass = _this.find(o.liclass);
                var aclass = _this.find(o.aclass);
                var item_media=_this.find(o.item_media);
                liclass.each(function (index, e) {
                    var NoticeId = aclass.eq(index).attr("data-info");
                    var url = '<?php echo U("Home/Notice/checkRead");?>';
                    $.post(url, {notice_id: NoticeId}, function (data) {
                        item_media.eq(index).prepend(data);
                    });
                });
            }
        })(jQuery);

        $("#list").IsRead();

    </script>

</body>
</html>