<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/5/6 0006
 * Time: 18:47
 */

/**
 * 获取access_token
 * @return string
 */

function getToken()
{
    $access_token = S("access_token");
    if ($access_token) {
        return $access_token;
    } else {
        vendor('Httpful.bootstrap');
        $url = joinParams('/gettoken', array('corpid' => C('dingtalk.CorpID'), 'corpsecret' => C('dingtalk.CorpSecret')));
        $response = \Httpful\Request::get($url)->send();
        $response = HttpResponse($response);
        $access_token = $response->access_token;
        S("access_token", $access_token, 7000);

        return $access_token;
    }
}

/**
 * 获取微应用后台管理免登SsoToken
 * @return string
 */
function getToken_Sso()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/sso/gettoken', array('corpid' => C('dingtalk.CorpID'), 'corpsecret' => C('dingtalk.SsoSecret')));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response->access_token;
}

function getToken_Sns()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/sns/gettoken', array('appid' => C('dingtalk.AppId'), 'appsecret' => C('dingtalk.AppSecret')));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response->access_token;
}

function getSnsToken($openid, $persistent_code)
{
    $sns_token = session("sns_token");
    if ($sns_token) {
        return $sns_token;
    } else {
        vendor('Httpful.bootstrap');
        $data = array(
            'openid' => $openid,
            'persistent_code' => $persistent_code
        );
        $url = joinParams('/sns/get_sns_token', array('access_token' => getToken_Sns()));
        $response = \Httpful\Request::post($url)
            ->body($data)
            ->sendsJson()
            ->send();
        $response = HttpResponse($response);
        $sns_token = $response->sns_token;
        session(array('sns_token' => $sns_token, 'expire' => 7100));
        return $response;
    }
}


/**
 * 获取部门列表
 * @author 小麦mak
 * @return array
 */

function getDepartmentList()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/department/list', array('access_token' => getToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response->department;
}

/**
 * 获取部门详情
 * @author 小麦mak
 * @param $id
 * @return \Httpful\Response|mixed
 */

function getDepartment($id)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/department/get', array('access_token' => getToken(), 'id' => $id));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 创建部门
 * @author 小麦mak
 * @param $name                     部门名称。长度限制为1~64个字符
 * @param $parentid                 父部门id。根部门id为1
 * @param string $order 在父部门中的次序值。order值小的排序靠前
 * @param bool $createDeptGroup 是否创建一个关联此部门的企业群，默认为false
 * @return array
 */

function createDepartment($name, $parentid, $order = '', $createDeptGroup = false)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'name' => $name,
        'parentid' => $parentid,
        'order' => $order,
        'createDeptGroup' => $createDeptGroup
    );
    $url = joinParams('/department/create', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

//function getSnsToken($openid,$persistent_code){
//    vendor('Httpful.bootstrap');
//    $data = array(
//        'openid' => $openid,
//        'persistent_code' => $persistent_code,
//    );
//    $url = joinParams('sns/get_sns_token', array('access_token' => getToken()));
//    $response = \Httpful\Request::post($url)
//        ->body($data)
//        ->sendsJson()
//        ->send();
//    $response = HttpResponse($response);
//    return $response;
//}

//function getUserInfo_Sns($SNS_TOKEN){
//    vendor('Httpful.bootstrap');
//    $url = joinParams('sns/getuserinfo', array('sns_token' => $SNS_TOKEN));
//    $response = \Httpful\Request::get($url)->send();
//    $response = HttpResponse($response);
//    return $response;
//}

/**
 * 获取用户授权的持久授权码
 * @param $tmp_auth_code
 * @return array
 */
//function getPersistentCode($tmp_auth_code){
//    vendor('Httpful.bootstrap');
//    $data = array(
//        'tmp_auth_code' => $tmp_auth_code,
//    );
//    $url = joinParams('sns/get_persistent_code', array('access_token' => getToken()));
//    $response = \Httpful\Request::post($url)
//        ->body($data)
//        ->sendsJson()
//        ->send();
//    $response = HttpResponse($response);
//    return $response;
//}

/**
 * 更新部门
 * @author 小麦mak
 * @param $id
 * @param string $name
 * @param string $parentid
 * @param string $order
 * @param bool $createDeptGroup
 * @param bool $autoAddUser
 * @param string $deptManagerUseridList
 * @param bool $deptHiding
 * @param string $deptPerimits
 * @param string $userPerimits
 * @param bool $outerDept
 * @param string $outerPermitDepts
 * @param string $outerPermitUsers
 * @param string $orgDeptOwner
 * @return array
 */

function updateDepartment($id, $name = '', $parentid = '', $order = '', $createDeptGroup = null, $autoAddUser = null, $deptManagerUseridList = '', $deptHiding = null, $deptPerimits = '', $userPerimits = '', $outerDept = null, $outerPermitDepts = '', $outerPermitUsers = '', $orgDeptOwner = '')
{
    vendor('Httpful.bootstrap');
    $data = array(
        'name' => $name,
        'parentid' => $parentid,
        'order' => $order,
        'id' => $id,
        'createDeptGroup' => $createDeptGroup,
        'autoAddUser' => $autoAddUser,
        'deptManagerUseridList' => $deptManagerUseridList,
        'deptHiding' => $deptHiding,
        'deptPerimits' => $deptPerimits,
        'userPerimits' => $userPerimits,
        'outerDept' => $outerDept,
        'outerPermitDepts' => $outerPermitDepts,
        'outerPermitUsers' => $outerPermitUsers,
        'orgDeptOwner' => $orgDeptOwner
    );
    $url = joinParams('/department/update', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 删除部门
 * @author 小麦mak
 * @param $id
 * @return \Httpful\Response|mixed
 */

function deleteDepartment($id)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/department/delete', array('access_token' => getToken(), 'id' => $id));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * @author 小麦mak
 * @param $userid
 * @return \Httpful\Response|mixed
 */

function getUser($userid)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/get', array('access_token' => getToken(), 'userid' => $userid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 创建成员
 * @author 小麦mak
 * @param string $name
 * @param array $department
 * @param string $mobile
 * @param string $userid
 * @param array $orderInDepts
 * @param string $position
 * @param string $tel
 * @param string $workPlace
 * @param string $remark
 * @param string $email
 * @param string $jobnumber
 * @param bool $isHide
 * @param bool $isSenior
 * @param array $extattr
 * @return mixed
 */

function createUser($name, $department, $mobile, $userid = '', $orderInDepts = array(), $position = '', $tel = '', $workPlace = '', $remark = '', $email = '', $jobnumber = '', $isHide = null, $isSenior = null, $extattr = array())
{
    vendor('Httpful.bootstrap');
    $data = array(
        'userid' => $userid,
        'name' => $name,
        'orderInDepts' => $orderInDepts,
        'department' => $department,
        'position' => $position,
        'mobile' => $mobile,
        'tel' => $tel,
        'workPlace' => $workPlace,
        'remark' => $remark,
        'email' => $email,
        'jobnumber' => $jobnumber,
        'isHide' => $isHide,
        'isSenior' => $isSenior,
        'extattr' => $extattr
    );
    $url = joinParams('/user/create', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 更新成员
 * @author 小麦mak
 * @param string $name
 * @param array $department
 * @param string $mobile
 * @param string $userid
 * @param array $orderInDepts
 * @param string $position
 * @param string $tel
 * @param string $workPlace
 * @param string $remark
 * @param string $email
 * @param string $jobnumber
 * @param bool $isHide
 * @param bool $isSenior
 * @param array $extattr
 * @return mixed
 */
function updateUser($name, $department = '', $mobile, $userid, $orderInDepts = array(), $position = '', $tel = '', $workPlace = '', $remark = '', $email = '', $jobnumber = '', $isHide = null, $isSenior = null, $extattr = array())
{
    vendor('Httpful.bootstrap');
    $data = array(
        'userid' => $userid,
        'name' => $name,
        'orderInDepts' => $orderInDepts,
        'department' => $department,
        'position' => $position,
        'mobile' => $mobile,
        'tel' => $tel,
        'workPlace' => $workPlace,
        'remark' => $remark,
        'email' => $email,
        'jobnumber' => $jobnumber,
        'isHide' => $isHide,
        'isSenior' => $isSenior,
        'extattr' => $extattr
    );
    $url = joinParams('/user/update', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 删除成员
 * @author 小麦mak
 * @param string $userid
 * @return \Httpful\Response|mixed
 */

function deleteUser($userid)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/delete', array('access_token' => getToken(), 'userid' => $userid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 批量删除成员
 * @author 小麦mak
 * @param array $useridlist
 * @return mixed
 */

function batchDeleteUser($useridlist)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/create', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($useridlist)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取部门成员
 * @author 小麦mak
 * @param string $department_id
 * @param long $offset
 * @param int $size
 * @param string $order
 * @return \Httpful\Response|mixed
 */
function getUserSimpleList($department_id, $offset = null, $size = null, $order = '')
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/simplelist', array('access_token' => getToken(), 'department_id' => $department_id, 'offset' => $offset, 'size' => $size, 'order' => $order));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取部门成员（详情）
 * @author 小麦mak
 * @param $department_id
 * @param long $offset
 * @param int $size
 * @param string $order
 * @return \Httpful\Response|mixed
 */
function getUserList($department_id, $offset = null, $size = null, $order = '')
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/simplelist', array('access_token' => getToken(), 'department_id' => $department_id, 'offset' => $offset, 'size' => $size, 'order' => $order));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 创建微应用
 * @author 小麦mak
 * @param $appIcon
 * @param $appName
 * @param $appDesc
 * @param $homepageUrl
 * @param string $pcHomepageUrl
 * @param string $ompLink
 * @return mixed
 */

function createMicroApp($appIcon, $appName, $appDesc, $homepageUrl, $pcHomepageUrl = '', $ompLink = '')
{
    vendor('Httpful.bootstrap');
    $data = array(
        'appIcon' => $appIcon,
        'appName' => $appName,
        'appDesc' => $appDesc,
        'homepageUrl' => $homepageUrl,
        'pcHomepageUrl' => $pcHomepageUrl,
        'ompLink' => $ompLink
    );
    $url = joinParams('/microapp/create', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取企业设置的微应用可见范围
 * @author 小麦mak
 * @param $agentId
 * @return mixed
 */
function visibleScopesMicroApp($agentId)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'agentId' => $agentId
    );
    $url = joinParams('/microapp/visible_scopes', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 创建会话
 * @author 小麦mak
 * @param $name
 * @param $owner
 * @param $useridlist
 * @return mixed
 */
function createChat($name, $owner, $useridlist)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'name' => $name,
        'owner' => $owner,
        'useridlist' => $useridlist
    );
    $url = joinParams('/chat/create', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 修改会话
 * @author 小麦mak
 * @param $chatid
 * @param string $name
 * @param string $owner
 * @param array $add_useridlist
 * @param array $del_useridlist
 * @return mixed
 */
function updateChat($chatid, $name = '', $owner = '', $add_useridlist = array(), $del_useridlist = array())
{
    vendor('Httpful.bootstrap');
    $data = array(
        'chatid' => $chatid,
        'name' => $name,
        'owner' => $owner,
        'add_useridlist' => $add_useridlist,
        'del_useridlist' => $del_useridlist
    );
    $url = joinParams('/chat/update', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取会话
 * @author 小麦mak
 * @param $chatid
 * @return \Httpful\Response|mixed
 */

function getChat($chatid)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/chat/get', array('access_token' => getAccessToken(), 'chatid' => $chatid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 绑定微应用和群会话
 * @author 小麦mak
 * @param $chatid
 * @param $agentid
 * @return array
 */

function bindChat($chatid, $agentid)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/chat/bind', array('access_token' => getAccessToken(), 'chatid' => $chatid, 'agentid' => $agentid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 解绑微应用和群会话
 * @author 小麦mak
 * @param $chatid
 * @param $agentid
 * @return array
 */

function unbindChat($chatid, $agentid)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/chat/unbind', array('access_token' => getAccessToken(), 'chatid' => $chatid, 'agentid' => $agentid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 发送消息到群会话
 * @author 小麦mak
 * @return array
 */

function sendChat($data)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/chat/send', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 发送文本消息
 * @author 小麦mak
 * @param $chatid
 * @param $sender
 * @param $msgtype
 * @param $content
 * @return array
 */

function sendTextChat($chatid, $sender, $content)
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "text",
        'text' => array(
            'content' => $content
        )
    );
    return sendChat($data);
}

/**
 * 发送图片消息
 * @author 小麦mak
 * @param $chatid
 * @param $sender
 * @param $msgtype
 * @param $content
 * @return array
 */

function sendImageChat($chatid, $sender, $media_id)
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "image",
        'image' => array(
            'media_id' => $media_id
        )
    );
    return sendChat($data);
}

/**
 * 发送语音消息
 * @author 小麦mak
 * @param $chatid
 * @param $sender
 * @param $msgtype
 * @param $media_id
 * @param $duration
 * @return array
 */

function sendVoiceChat($chatid, $sender, $media_id, $duration)
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "voice",
        'voice' => array(
            'media_id' => $media_id,
            'duration' => $duration
        )
    );
    return sendChat($data);
}

/**
 * 发送文件消息
 * @param $chatid
 * @param $sender
 * @param $msgtype
 * @param $media_id
 * @return array
 */

function sendFileChat($chatid, $sender, $media_id)
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "file",
        'file' => array(
            'media_id' => $media_id
        )
    );
    return sendChat($data);
}

/**
 * 发送图文消息
 * @author 小麦mak
 * @param $chatid
 * @param $sender
 * @param $title
 * @param $text
 * @param $pic_url
 * @param $message_url
 * @return array
 */

function sendLinkChat($chatid, $sender, $title, $text, $pic_url, $message_url)
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "link",
        'link' => array(
            'title' => $title,
            'text' => $text,
            'pic_url' => $pic_url,
            'message_url' => $message_url
        )
    );
    return sendChat($data);
}


/**
 * 发送OA消息
 * @param $chatid
 * @param $sender
 * @param $message_url
 * @param $pc_message_url
 * @param $bgcolor
 * @param $text
 * @param string $title
 * @param array $form
 * @param string $num
 * @param string $unit
 * @param string $content
 * @param string $image
 * @param string $file_count
 * @param string $author
 * @return array
 */

function sendOAChat($chatid, $sender, $message_url, $pc_message_url, $bgcolor, $text, $title = "", $form = array(array()), $num = "", $unit = "", $content = "", $image = "", $file_count = "", $author = "")
{
    $data = array(
        'chatid' => $chatid,
        'sender' => $sender,
        'msgtype' => "oa",
        'oa' => array(
            'message_url' => $message_url,
            'pc_message_url' => $pc_message_url,
            'head' => array(
                'bgcolor' => $bgcolor,
                'text' => $text
            ),
            'body' => array(
                'title' => $title,
                'form' => $form,
                'rich' => array(
                    'num' => $num,
                    'unit' => $unit
                ),
                'content' => $content,
                'image' => $image,
                'file_count' => $file_count,
                'author' => $author
            ),
        )
    );
    return sendChat($data);
}


/**
 * 注册事件回调
 * @author 小麦mak
 * @param array $call_back_tag
 * @param $token
 * @param $aes_key
 * @param $url
 * @return mixed
 */

function registerCallBack($call_back_tag, $token, $aes_key, $url)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'call_back_tag' => $call_back_tag,
        'token' => $token,
        'aes_key' => $aes_key,
        'url' => $url
    );
    $jurl = joinParams('/call_back/register_call_back', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::post($jurl)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 查询事件回调
 * @author 小麦mak
 * @return \Httpful\Response|mixed
 */

function getCallBack()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/call_back/get_call_back', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 更新事件回调
 * @author 小麦mak
 * @param array $call_back_tag
 * @param $token
 * @param $aes_key
 * @param $url
 * @return mixed
 */

function updateCallBack($call_back_tag, $token, $aes_key, $url)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'call_back_tag' => $call_back_tag,
        'token' => $token,
        'aes_key' => $aes_key,
        'url' => $url
    );
    $jurl = joinParams('/call_back/update_call_back', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::post($jurl)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 删除事件回调
 * @author 小麦mak
 * @return \Httpful\Response|mixed
 */

function deleteCallBack()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/call_back/get_call_back', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取回调失败的结果
 * @author 小麦mak
 * @return \Httpful\Response|mixed
 */

function failedCallBack()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/call_back/get_call_back_failed_result', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 发送普通会话消息
 * @author 小麦mak
 * @param array $data
 * @return array
 */

function sendToConversation($data)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/message/send_to_conversation', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}


/**
 * 发送普通文本会话消息
 * @author 小麦mak
 * @param $sender
 * @param $content
 * @return array
 */
function sendTextConversation($sender, $cid, $content)
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "text",
        'text' => array(
            'content' => $content
        )
    );
    return sendConversation($data);
}


/**
 * 发送普通图片会话消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @return array
 */
function sendImageConversation($sender, $cid, $media_id)
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "image",
        'image' => array(
            'media_id' => $media_id
        )
    );
    return sendConversation($data);
}


/**
 * 发送普通语音会话消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @param $duration
 * @return array
 */
function sendVoiceConversation($sender, $cid, $media_id, $duration)
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "voice",
        'voice' => array(
            'media_id' => $media_id,
            'duration' => $duration
        )
    );
    return sendConversation($data);
}


/**
 * 发送普通文件会话消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @return array
 */
function sendFileConversation($sender, $cid, $media_id)
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "file",
        'file' => array(
            'media_id' => $media_id
        )
    );
    return sendConversation($data);
}


/**
 * 发送普通图文会话消息
 * @author 小麦mak
 * @param $sender
 * @param $title
 * @param $text
 * @param $pic_url
 * @param $message_url
 * @return array
 */
function sendLinkConversation($sender, $cid, $title, $text, $pic_url, $message_url)
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "link",
        'link' => array(
            'title' => $title,
            'text' => $text,
            'pic_url' => $pic_url,
            'message_url' => $message_url
        )
    );
    return sendConversation($data);
}


/**
 * 发送普通OA会话消息
 * @author 小麦mak
 * @param $sender
 * @param $message_url
 * @param $pc_message_url
 * @param $bgcolor
 * @param $text
 * @param string $title
 * @param array $form
 * @param string $num
 * @param string $unit
 * @param string $content
 * @param string $image
 * @param string $file_count
 * @param string $author
 * @return array
 */
function sendOAConversation($sender, $cid, $message_url, $pc_message_url, $bgcolor, $text, $title = "", $form = array(array()), $num = "", $unit = "", $content = "", $image = "", $file_count = "", $author = "")
{
    $data = array(
        'sender' => $sender,
        'cid' => $cid,
        'msgtype' => "oa",
        'oa' => array(
            'message_url' => $message_url,
            'pc_message_url' => $pc_message_url,
            'head' => array(
                'bgcolor' => $bgcolor,
                'text' => $text
            ),
            'body' => array(
                'title' => $title,
                'form' => $form,
                'rich' => array(
                    'num' => $num,
                    'unit' => $unit
                ),
                'content' => $content,
                'image' => $image,
                'file_count' => $file_count,
                'author' => $author
            ),
        )
    );
    return sendConversation($data);
}

/**
 * 发送企业消息
 * @author 小麦mak
 * @param $data
 * @return mixed
 */
function sendMessage($data)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/message/send', array('access_token' => getAccessToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 发送企业文本消息
 * @author 小麦mak
 * @param $sender
 * @param $content
 * @return array
 */
function sendTextMessage($content)
{
    $data = array(
        'msgtype' => "text",
        'text' => array(
            'content' => $content
        )
    );
    return sendMessage($data);
}


/**
 * 发送企业图片消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @return array
 */
function sendImageMessage($media_id)
{
    $data = array(
        'msgtype' => "image",
        'image' => array(
            'media_id' => $media_id
        )
    );
    return sendMessage($data);
}


/**
 * 发送企业语音消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @param $duration
 * @return array
 */
function sendVoiceMessage($media_id, $duration)
{
    $data = array(
        'msgtype' => "voice",
        'voice' => array(
            'media_id' => $media_id,
            'duration' => $duration
        )
    );
    return sendMessage($data);
}


/**
 * 发送企业文件消息
 * @author 小麦mak
 * @param $sender
 * @param $media_id
 * @return array
 */
function sendFileMessage($media_id)
{
    $data = array(
        'msgtype' => "file",
        'file' => array(
            'media_id' => $media_id
        )
    );
    return sendMessage($data);
}


/**
 * 发送企业图文消息
 * @author 小麦mak
 * @param $sender
 * @param $title
 * @param $text
 * @param $pic_url
 * @param $message_url
 * @return array
 */
function sendLinkMessage($title, $text, $pic_url, $message_url)
{
    $data = array(
        'msgtype' => "link",
        'link' => array(
            'title' => $title,
            'text' => $text,
            'pic_url' => $pic_url,
            'message_url' => $message_url
        )
    );
    return sendMessage($data);
}


/**
 * 发送企业OA消息
 * @author 小麦mak
 * @param $sender
 * @param $message_url
 * @param $pc_message_url
 * @param $bgcolor
 * @param $text
 * @param string $title
 * @param array $form
 * @param string $num
 * @param string $unit
 * @param string $content
 * @param string $image
 * @param string $file_count
 * @param string $author
 * @return array
 */
function sendOAMessage($message_url, $pc_message_url, $bgcolor, $text, $title = "", $form = array(array()), $num = "", $unit = "", $content = "", $image = "", $file_count = "", $author = "")
{
    $data = array(
        'msgtype' => "oa",
        'oa' => array(
            'message_url' => $message_url,
            'pc_message_url' => $pc_message_url,
            'head' => array(
                'bgcolor' => $bgcolor,
                'text' => $text
            ),
            'body' => array(
                'title' => $title,
                'form' => $form,
                'rich' => array(
                    'num' => $num,
                    'unit' => $unit
                ),
                'content' => $content,
                'image' => $image,
                'file_count' => $file_count,
                'author' => $author
            ),
        )
    );
    return sendMessage($data);
}

/**
 * 上传媒体文件
 * @author 小麦mak
 * TODO 还没想好
 */
function uploadMedia()
{

}

/**
 * 获取媒体文件
 * @author 小麦mak
 * @param $media_id
 * @return \Httpful\Response|mixed
 */
function getMedia($media_id)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/media/get', array('access_token' => getToken(), 'media_id' => $media_id));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}


/**
 * 通过CODE换取用户身份
 * @author 小麦mak
 * @param $code
 * @return \Httpful\Response|mixed
 */
function getUserInfo($code)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/getuserinfo', array('access_token' => getToken(), 'code' => $code));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}


/**
 * 通过CODE换取微应用管理员的身份信息
 * @param $code
 * @return \Httpful\Response|mixed
 */
function getUserInfo_Sso($code)
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/sso/getuserinfo', array('access_token' => getToken_Sso(), 'code' => $code));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}

function getPersistentCode($tmp_auth_code)
{
    vendor('Httpful.bootstrap');
    $data = array(
        'tmp_auth_code' => $tmp_auth_code,
    );
    $url = joinParams('/sns/get_persistent_code', array('access_token' => getToken_Sns()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

function getUserInfo_Sns()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/getuserinfo', array('sns_token' => getSnsToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response;
}


/**
 * 记录统计数据
 * @author 小麦mak
 * @param $startTimeMs
 * @param $endTimeMs
 * @param $userid
 * @param $agentId
 * @param $callbackUrl
 * @param string $module
 * @param string $originId
 * @param array $extension
 * @return mixed
 */
function recordData($startTimeMs, $endTimeMs, $userid, $agentId, $callbackUrl, $module = '', $originId = '', $extension = array())
{
    vendor('Httpful.bootstrap');
    $data = array(
        'startTimeMs' => $startTimeMs,
        'endTimeMs' => $endTimeMs,
        'module' => $module,
        'originId' => $originId,
        'userid' => $userid,
        'agentId' => $agentId,
        'callbackUrl' => $callbackUrl,
        'extension' => $extension
    );
    $url = joinParams('/data/record', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 更新统计数据
 * @author 小麦mak
 * @param $id
 * @param $startTimeMs
 * @param $endTimeMs
 * @param $userid
 * @param $agentId
 * @param $callbackUrl
 * @param string $module
 * @param string $originId
 * @param array $extension
 * @return mixed
 */
function updateData($id, $startTimeMs, $endTimeMs, $userid, $agentId, $callbackUrl, $module = '', $originId = '', $extension = array())
{
    vendor('Httpful.bootstrap');
    $data = array(
        'id' => $id,
        'startTimeMs' => $startTimeMs,
        'endTimeMs' => $endTimeMs,
        'module' => $module,
        'originId' => $originId,
        'userid' => $userid,
        'agentId' => $agentId,
        'callbackUrl' => $callbackUrl,
        'extension' => $extension
    );
    $url = joinParams('/data/update', array('access_token' => getToken()));
    $response = \Httpful\Request::post($url)
        ->body($data)
        ->sendsJson()
        ->send();
    $response = HttpResponse($response);
    return $response;
}

/**
 * 获取jsapi_ticket
 * @author 小麦mak
 * @return \Httpful\Response|mixed
 */
function getJsapiTicket()
{
    vendor('Httpful.bootstrap');
    $url = joinParams('/get_jsapi_ticket', array('access_token' => getToken()));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response->ticket;
}

//function DingOauth($url){
//    vendor('Httpful.bootstrap');
//
////    $response = \Httpful\Request::get($url)->send();
////    $response = HttpResponse($response);
////    redirect($url);
////    return $response;
//    $url='https://oa.dingtalk.com/omp/api/micro_app/admin/landing?corpid='.C('dingtalk.CorpID').'&redirect_url='.urldecode($url);
//    return $url;
//}
/**
 * JsApi权限验证配置
 * @param $corpId
 * @return array
 */
function isvConfig()
{
    // 注意 URL 一定要动态获取，不能 hardcode.
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $timeStamp = time();
//    $nonceStr =createNonceStr();
    $nonceStr = "abcdefg";

    $ticket = getJsapiTicket();

    $plain = 'jsapi_ticket=' . $ticket .
        '&noncestr=' . $nonceStr .
        '&timestamp=' . $timeStamp .
        '&url=' . $url;

    $signature = sha1($plain);

    $isvConfig = array(
        "url" => $url,
        "agentId" => C("dingtalk.AgentID"),
        "corpId" => C("dingtalk.CorpID"),
        "timeStamp" => $timeStamp,
        "nonceStr" => $nonceStr,
        "signature" => $signature,
    );
    return $isvConfig;
}

/**
 * 生成签名的随机串
 * @param int $length
 * @return string
 */
function createNonceStr($length = 16)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
}

/**
 * 获取http结果
 * @author 小麦mak
 * @param $response
 * @return mixed
 */

function HttpResponse($response)
{
    if ($response->hasErrors()) {
        var_dump($response);
    }

    if ($response->body->errcode != 0) {
        var_dump($response->body);
    }
    return $response->body;
}


/**
 * 获取拼接后url
 * @author 小麦mak
 * @param $path  api的路径
 * @param $params   需要传输的参数
 * @return string
 *
 */
function joinParams($path, $params)
{
    $url = C('dingtalk.OAPI_HOST') . $path;
    if (count($params) > 0) {
        $url = $url . "?";
        foreach ($params as $key => $value) {
            $url = $url . $key . "=" . $value . "&";
        }
        $length = count($url);
        if ($url[$length - 1] == '&') {
            $url = substr($url, 0, $length - 1);
        }
    }
    return $url;
}


/**
 *  获取当前访问的完整url地址
 * @return string
 */
//function GetCurUrl() {
//    $url = 'http://';
//    if (isset ( $_SERVER ['HTTPS'] ) && $_SERVER ['HTTPS'] == 'on') {
//        $url = 'https://';
//    }
//    if ($_SERVER ['SERVER_PORT'] != '80') {
//        $url .= $_SERVER ['HTTP_HOST'] . ':' . $_SERVER ['SERVER_PORT'] . $_SERVER ['REQUEST_URI'];
//    } else {
//        $url .= $_SERVER ['HTTP_HOST'] . $_SERVER ['REQUEST_URI'];
//    }
//    // 兼容后面的参数组装
//    if (stripos ( $url, '?' ) === false) {
//        $url .= '?t=' . time ();
//    }
//    return $url;
//}

/**
 * 获取当前页面的url
 * @return string
 */

function curPageURL()
{
    $pageURL = 'http';
    if (array_key_exists('HTTPS', $_SERVER) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

/**
 * 钉钉oauth免登
 * @auther 小麦mak
 * @param $burl
 */
function OauthDingTalk($burl){
    if (!session("?openid")){
        $code=I("code");
        if ($code){
            $PersistentCode=getPersistentCode($code);
            session("openid",$PersistentCode->openid);
//            dump($PersistentCode);
            session("persistent_code",$PersistentCode->persistent_code);
            session("unionid",$PersistentCode->unionid);
        }else{
            $param['appid']=C("dingtalk.AppId");
            $param['response_type']="code";
            $param['scope']="snsapi_login";
            $param['state']="1234";
            $param['redirect_uri']=urlencode($burl);
            $url=joinParams("/connect/oauth2/sns_authorize",$param);
            redirect($url);
//            echo $url;
        }
    }
}


/**
 * 根据unionid获取成员的userid
 * @auther 小麦mak
 * @param $unionid
 * @return mixed
 */
function getUseridByUnionid($unionid){
    vendor('Httpful.bootstrap');
    $url = joinParams('/user/getUseridByUnionid', array('access_token' => getToken(),'unionid'=>$unionid));
    $response = \Httpful\Request::get($url)->send();
    $response = HttpResponse($response);
    return $response->userid;
}

//function getThereMap($latitude,$longitude){
//    $url="http://restapi.amap.com/v3/staticmap?location={$latitude},{$longitude}&zoom=17&size=750*300&markers=mid,,A:{$latitude},{$longitude}&key=ee95e52bf08006f63fd29bcfbcf21df0";
//    return $url;
//}

/**
 * 获取公告阅读人数
 * @param $notice_id
 * @return mixed
 */

function getReadNum($notice_id){
    if (!empty($notice_id)){
        $read_notice=M("read_notice");
        $num=$read_notice->where("notice_id=$notice_id")->count();
        return $num;
    }
}


/**
 * 上传配置
 * @return array|bool
 */
function upload(){
    $config = array(
        'maxSize'    =>    314572800000,
        'rootPath'   =>    './Uploads/',
        'savePath'   =>    '',
        'saveName'   =>    array('uniqid',''),
        'autoSub'    =>    true,
        'subName'    =>    array('date','Ymd'),
    );
    $upload=new \Think\Upload($config);
    return $upload->upload();
}

/**
 * 统计今日签到次数
 * @return mixed
 */
function getSignNum(){
    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
    $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;

    $Sign=M("sign");
    $where['unionid']=session("unionid");
    $where['time']=array(array('egt',$beginToday),array('elt',$endToday));
    $num=$Sign->where($where)->count();
    return $num;
}

function getLeaveName($class){
    if (!empty($class)){
        $LeaveClass=M("leave_class");
        $name=$LeaveClass->where("classid={$class}")->getField('name');
        return $name;
    }
}

function getUserName($unionid){
    return getUser(getUseridByUnionid($unionid))->name;
}

function getBoardroomName($id){
    $Boardroom=M("boardroom");
    $name=$Boardroom->where("id=$id")->getField("name");
    return $name;

}

function isDingTalkAdmin($unionid){
    $isAdmin=getUser(getUseridByUnionid($unionid))->isAdmin;
    return $isAdmin;
}