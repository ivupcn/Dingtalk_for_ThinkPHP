<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/6/23 0023
 * Time: 23:09
 */

namespace Home\Controller;

class LogController extends ComController
{
    public function index()
    {
        $unionid = session("unionid");
        $JsConfig = isvConfig();
        $this->assign("_config", $JsConfig);
        $p = empty($_GET['p']) ? 0 : $_GET['p'];
        $Log = M("log");
        $list = $Log->where(array("unionid" => $unionid))->order('addtime desc')->page($p . ',10')->select();
        $this->assign('list', $list);
        $count = $Log->where(array("unionid" => $unionid))->count();
        $Page = new \Think\Page($count, 10);
        $Page->setConfig('prev', '上一页');
        $Page->setConfig('next', '下一页');
        $show = $Page->show();
        $this->assign('page', $show);
        $this->display();
    }

    public function others()
    {
        $JsConfig = isvConfig();
        $this->assign("_config", $JsConfig);
        $unionid = session("unionid");
        $where["share"] = 1;
        $where["unionid"] = array("neq", $unionid);
        $p = empty($_GET['p']) ? 0 : $_GET['p'];
        $Log = M("log");

        $list = $Log->where($where)->order('addtime desc')->page($p . ',10')->select();
        $this->assign('list', $list);
        $count = $Log->where($where)->count();
        $Page = new \Think\Page($count, 10);
        $Page->setConfig('prev', '上一页');
        $Page->setConfig('next', '下一页');
        $show = $Page->show();
        $this->assign('page', $show);
        $this->display();
    }

    public function add()
    {
        $JsConfig = isvConfig();
        $this->assign("_config", $JsConfig);

        $unionid = session("unionid");
        if (IS_POST) {
            $data['log_class'] = I("post.log_class");
            $data['title'] = I("post.title");
            $data['content'] = I("post.content");
            $data['unionid'] = $unionid;
            $data['share'] = I("post.share") == "on" ? 1 : 0;
            $data['time'] = strtotime(I("post.time"));
            $data['addtime'] = time();
            $Log = M("log");
            if ($Log->add($data)) {
                $this->success("添加成功",U('index')."?dd_nav_bgcolor=FF5E97F6",3);
            } else {
                $this->error("添加失败",U('add')."?dd_nav_bgcolor=FF5E97F6",3);
            }
        } else {
            $this->display();
        }
    }
}