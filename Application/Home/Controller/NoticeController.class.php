<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/5/28 0028
 * Time: 18:43
 */

namespace Home\Controller;

/**
 * 公告
 * Class NoticeController
 * @package Home\Controller
 */

class NoticeController extends ComController
{
    public function index()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $notice = M('notice');
        $count = $notice->count();
        $Page = new \Think\Page($count, 5);
        $Page->setConfig('prev', '上一页');
        $Page->setConfig('next', '下一页');
        $show = $Page->show();
        $list = $notice->order('time desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display(); // 输出模板
    }

    public function details($id)
    {
        $notice = M("notice");
        $ret = $notice->where("id=$id")->find();
        $this->assign("ret", $ret);

        $read_notice = M("read_notice");
        $data["unionid"] = session("unionid");
        $data["notice_id"] = $id;
        $data["name"] = getUserName($data["unionid"]);
//        $data["name"] = 2;
        $data["time"] = time();

        $where["unionid"] = $data["unionid"];
        $where["notice_id"] = $data["notice_id"];
        $ret = $read_notice->where($where)->count();
        if ($ret <= 0) {
            $read_notice->add($data);
        }
        $this->display();
    }

    public function read_people($notice_id)
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $read_notice = M("read_notice");
        $ret = $read_notice->where("notice_id=$notice_id")->order("time desc")->select();
        $this->assign("list", $ret);
        $this->display();
    }

    public function checkRead()
    {
        $where['unionid'] = session("unionid");
        $where['notice_id'] = I("post.notice_id");
        if ($where != null) {
            $Read = M("read_notice");
            $ret = $Read->where($where)->find();
            if ($ret > 0) {
                $html = "<i class='iconfont icon-yidu'></i>";
                $this->ajaxReturn($html);
            } else {
                $html = "<i class='iconfont icon-weidu'></i>";
                $this->ajaxReturn($html);
            }
        }
    }

    public function addNotice()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);
        if (IS_POST) {
            $unionid=session("unionid");
            $data['title']=I("post.title");
            $data['content']=I("post.content");
            $data['unionid']=$unionid;
            $data['username']=getUserName($unionid);
            $data['time']=time();

            $Notice=M("notice");
            if ($Notice->add($data)){
                $this->success("提交成功",U("index")."?dd_nav_bgcolor=FF5E97F6",2);
            }else{
                $this->error("提交失败");
            }
        } else {
            $this->display();
        }
    }
}