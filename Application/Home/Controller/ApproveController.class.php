<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/6/1 0001
 * Time: 18:13
 */
namespace Home\Controller;

/**
 * 审批功能
 * Class ApproveController
 * @package Home\Controller
 */

class ApproveController extends ComController
{

    public function _initialize()
    {
        define("LEAVEID",0);//请假为0
        define("BUSINESSID",1);//出差为1
        define("REIMBURSEID",2);//报销为2
    }

    /**
     * 审批首页
     */

    public function index()
    {
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);
        $this->display();
    }


    /**
     * 请假
     */
    public function leave(){
        //jsapi配置
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $LeaveClass=M("leave_class");
        $ret=$LeaveClass->select();
        $this->assign("class",$ret);
        $this->display();
    }

    /**
     * 提交请假
     */
    public function addLeave(){
        $unionid=session("unionid");
        $info=upload();

        $data['name']=getUser(getUseridByUnionid($unionid))->name;
        $data['class']=I("post.class");
        $data['start']=strtotime(I("post.start"));
        $data['end']=strtotime(I("post.end"));
        $data['how']=I("post.how");
        $data['why']=I("post.why");
        $data['pic']=C("file_upload").$info['pic']['savepath'].$info['pic']['savename'];

        $userid=I("post.userid");
        $approve['unionid']=$unionid;
        $approve['userid']=join(",",$userid);
        $approve['addtime']=time();
        $approve['approve_class']=constant("LEAVEID");

        $Leave=M("leave");
        $ret=$Leave->add($data);
        if ($ret){
            $approve['approve_id']=$ret;

            $App=M("approve");
            if ($App->add($approve)){
                $this->success("提交成功",U("Home/Approve/myApprove"));
            }
        }else{
            $this->error("提交失败，请重新提交！");
        }
    }


    /**
     * 出差
     */
    public function business(){
        //jsapi配置
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $this->display();
    }

    /**
     * 提交出差
     */
    public function addBusiness(){
        $unionid=session("unionid");
        $info=upload();

        $data['name']=getUser(getUseridByUnionid($unionid))->name;
        $data['address']=I("post.address");
        $data['start']=strtotime(I("post.start"));
        $data['end']=strtotime(I("post.end"));
        $data['how']=I("post.how");
        $data['why']=I("post.why");
        $data['pic']=C("file_upload").$info['pic']['savepath'].$info['pic']['savename'];

        $userid=I("post.userid");
        $approve['unionid']=$unionid;
        $approve['userid']=join(",",$userid);
        $approve['addtime']=time();
        $approve['approve_class']=constant("BUSINESSID");

        $Business=M("business");
        $ret=$Business->add($data);
        if ($ret){
            $approve['approve_id']=$ret;

            $App=M("approve");
            if ($App->add($approve)){
                $this->success("提交成功",U("Home/Approve/myApprove"));
            }
        }else{
            $this->error("提交失败，请重新提交！");
        }
    }

    /**
     * 报销
     */
    public function reimburse(){
        //jsapi配置
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);
        
        $this->display();
    }

    /**
     * 提交报销
     */
    public function addReimburse(){

        $unionid=session("unionid");
        $info=upload();

        $data['name']=getUser(getUseridByUnionid($unionid))->name;
        $data['money']=I("post.money");
        $data['classname']=I("post.classname");
        $data['details']=I("post.details");
        $data['pic']=C("file_upload").$info['pic']['savepath'].$info['pic']['savename'];

        $userid=I("post.userid");
        $approve['unionid']=$unionid;
        $approve['userid']=join(",",$userid);
        $approve['addtime']=time();
        $approve['approve_class']=constant("REIMBURSEID");

        $Reimburse=M("reimburse");
        $ret=$Reimburse->add($data);
        if ($ret){
            $approve['approve_id']=$ret;

            $App=M("approve");
            if ($App->add($approve)){
                $this->success("提交成功",U("Home/Approve/myApprove")."?dd_nav_bgcolor=FF5E97F6");
            }
        }else{
            $this->error("提交失败，请重新提交！");
        }
    }

    /**
     * 我发起的审批
     */
    public function myApprove(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        $Approve=M("approve");

        $where['unionid']=$unionid;
        $ret=$Approve->where($where)->order("addtime desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 已审批
     */
    public function alreadyApprove(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        $Approve=M("approve");

        $where['unionid']=$unionid;
        $where['allow']=array('neq',0);
        $ret=$Approve->where($where)->order("addtime desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 未审批
     */
    public function notApprove(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        $Approve=M("approve");

        $where['unionid']=$unionid;
        $where['allow']=0;
        $ret=$Approve->where($where)->order("addtime desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 待我审批
     */
    public function waiting(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        $Approve=M("approve");
        $userid=getUseridByUnionid($unionid);
        $where['userid']=array('like', "%$userid%");
        $where['allow']=0;
        $ret=$Approve->where($where)->order("addtime desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 我已审批
     */
    public function iApprove(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $unionid=session("unionid");
        $Approve=M("approve");

        $where['allower']=$unionid;
        $where['allow']=1;
        $ret=$Approve->where($where)->order("addtime desc")->select();
        $this->assign("list",$ret);
        $this->display();
    }

    /**
     * 审批详细内容
     */
    public function details(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        $class=I("get.class");
        $approve_id=I("get.approve_id");
        switch ($class){
            case 0:
                $Leave=M("leave");
                $ret=$Leave->join(C('DB_PREFIX')."approve ON ".C('DB_PREFIX')."leave.id = ".C('DB_PREFIX')."approve.approve_id")
                    ->where(array(C('DB_PREFIX')."approve.approve_id"=>$approve_id,C('DB_PREFIX')."approve.approve_class"=>0,C('DB_PREFIX')."leave.id"=>$approve_id))
                    ->find();
                break;
            case 1:
                $Business=M("business");
                $ret=$Business->join(C('DB_PREFIX')."approve ON ".C('DB_PREFIX')."business.id = ".C('DB_PREFIX')."approve.approve_id")
                    ->where(array(C('DB_PREFIX')."approve.approve_id"=>$approve_id,C('DB_PREFIX')."approve.approve_class"=>1,C('DB_PREFIX')."business.id"=>$approve_id))
                    ->find();
                break;
            case 2:
                $Reimburse=M("reimburse");
                $ret=$Reimburse->join(C('DB_PREFIX')."approve ON ".C('DB_PREFIX')."reimburse.id = ".C('DB_PREFIX')."approve.approve_id")
                    ->where(array(C('DB_PREFIX')."approve.approve_id"=>$approve_id,C('DB_PREFIX')."approve.approve_class"=>2,C('DB_PREFIX')."reimburse.id"=>$approve_id))
                    ->find();
                break;
        }
        $this->assign("ret",$ret);

        $this->display();
    }

    /**
     * 提交是否同意
     */
    public function submit(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);

        if(IS_POST){
            $unionid=session("unionid");
            $id=I('post.id');
            $data['allow']=I('post.allow');
            $data['allower']=$unionid;
            $data['allownote']=I('post.allownote');
            $data['addtime']=time();
            $App=M("approve");
            $ret=$App->where("id={$id}")->save($data);
            if ($ret){
                $this->success("提交成功",U("Home/Approve/iApprove"));
            }else{
                $this->error("提交失败，请重新提交！");
            }
        }else{
            $id=I('get.id');
            $allow=I('get.allow');
            $this->assign("id",$id)->assign("allow",$allow);
            $this->display();
        }
    }
    
    
}