<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags-->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Your app title -->
    <title>签到</title>
    <!-- Path to Framework7 Library CSS, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.min.css">
    <!-- Path to Framework7 color related styles, iOS Theme -->
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/framework7.ios.colors.min.css">
    <!-- Path to your custom app styles-->
    <link rel="stylesheet" href="<?php echo C('iconfont');?>">
    <link rel="stylesheet" href="/dingtalk/Public/Home/Framework7/css/my-app.css">
    
</head>
<body>



<!-- Status bar overlay for full screen mode (PhoneGap) -->
<div class="statusbar-overlay"></div>
<!-- Views -->
<div class="views">
    <!-- Your main view, should have "view-main" class -->
    <div class="view view-main">
        <!-- Top Navbar-->
        <div class="navbar">
            <div class="navbar-inner navbar-on-center">
                <div class="left sliding" style="transform: translate3d(0px, 0px, 0px);">
                    
                        <a href="javascript:" onclick="history.back();" class="back link">
                            <i class="icon icon-back" style="transform: translate3d(0px, 0px, 0px);"></i>
                            <span class="">返回</span>
                        </a>
                    

                </div>
                <div class="center sliding" style="left: -6.5px; transform: translate3d(0px, 0px, 0px);">签到</div>
                    <div class="right">
                        
    <a href="#" class="link external">
        <span class="">统计</span>
    </a>

                    </div>
            </div>
        </div>
        <!-- Pages container, because we use fixed-through navbar and toolbar, it has additional appropriate classes-->
        <div class="pages navbar-through toolbar-through">
            <!-- Page, "data-page" contains page name -->
            <div class="page">
                <!-- Scrollable page content -->
                <div class="page-content">
                    

    <img id="staticmap" class="lazy lazy-fadeIn ks-demo-lazy img-responsive " src="http://restapi.amap.com/v3/staticmap?location=116.481485,39.990464&zoom=10&size=750*300&markers=mid,,A:116.481485,39.990464&key=ee95e52bf08006f63fd29bcfbcf21df0
">
    <div class="list-block">
        <form method="post" enctype="multipart/form-data" action="<?php echo U('add');?>">
            <ul>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">今日签到次数</div>
                        <div class="item-after"><span class="badge color-green"><?php echo getSignNum();?></span></div>
                    </div>
                </li>
            </ul>
            <br>
            <ul>
                <li class="align-top">
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">当前地址<br><a href="#" id="selectMap">地址不对?</a></div>
                            <div class="item-input">
                                <textarea id="address" name="address" readonly></textarea>
                                <input type="hidden" id="latitude" name="latitude">
                                <input type="hidden" id="longitude" name="longitude">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>

            <br>
            <ul>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">现在时间</div>
                            <div class="item-input">
                                <input type="text" value="<?php echo date('Y-m-d h:i:s',$NowTime);?>">
                                <input type="hidden" name="time" value="<?php echo ($NowTime); ?>">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">签到备注</div>
                            <div class="item-input">
                                <textarea placeholder="请填写签到备注" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="align-top">
                    <div class="item-content">
                        <div class="item-inner">
                            <div class="item-title label">图片</div>
                            <div class="item-input">
                                <input type="file" name="pic" style="padding-top: 10px;">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="content-block">
                <input class="button button-big button-fill color-blue" type="submit" value="马上签到">
            </div>
        </form>
    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- Path to Framework7 Library JS-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/framework7.min.js"></script>
<!-- Path to your app js-->
<script type="text/javascript" src="/dingtalk/Public/Home/Framework7/js/my-app.js"></script>
<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/dingtalk/Public/Home/js/jquery.min.js"></script>
<!--<![endif]-->
<!--[if lte IE 8 ]>
<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js"></script>
<![endif]-->

    <script src="http://g.alicdn.com/ilw/ding/0.9.2/scripts/dingtalk.js"></script>
    <script type="text/javascript">
        dd.config({
            agentId: '<?php echo ($_config["agentId"]); ?>', // 必填，微应用ID
            corpId: '<?php echo ($_config["corpId"]); ?>',//必填，企业ID
            timeStamp: '<?php echo ($_config["timeStamp"]); ?>', // 必填，生成签名的时间戳
            nonceStr: '<?php echo ($_config["nonceStr"]); ?>', // 必填，生成签名的随机串
            signature: '<?php echo ($_config["signature"]); ?>', // 必填，签名
            jsApiList: ['device.geolocation.get', 'biz.map.search', 'biz.map.view'] // 必填，需要使用的jsapi列表
        });
        dd.ready(function () {
            dd.device.geolocation.get({
                targetAccuracy: 200,
                coordinate: 0,
                withReGeocode: true,
                onSuccess: function (result) {
                    $("#address").append(result.address);
                    $("#latitude").val(result.latitude);
                    $("#longitude").val(result.longitude);
                    $("#staticmap").attr("src", "http://restapi.amap.com/v3/staticmap?location=" + result.longitude + "," + result.latitude + "&zoom=17&size=750*300&markers=mid,,A:" + result.longitude + "," + result.latitude + "&key=ccba00e34a84c9c12c878557f4d29a40");
//                    dd.biz.map.view({
//                        latitude: result.latitude, // 纬度
//                        longitude: result.longitude, // 经度
//                        title: result.road // 地址/POI名称
//                    });

                },
                onFail: function (err) {
                }
            });

            $("#selectMap").click(function () {
                dd.biz.map.search({
//                    latitude: 39.903578, // 纬度
//                    longitude: 116.473565, // 经度
                    scope: 500, // 限制搜索POI的范围；设备位置为中心，scope为搜索半径

                    onSuccess: function (poi) {
                        $("#address").html("");
                        $("#address").append(poi.title);
                        $("#latitude").val(poi.latitude);
                        $("#longitude").val(poi.longitude);
                        $("#staticmap").attr("src", "http://restapi.amap.com/v3/staticmap?location=" + poi.longitude + "," + poi.latitude + "&zoom=16&size=750*300&markers=mid,,A:" + poi.longitude + "," + poi.latitude + "&key=ccba00e34a84c9c12c878557f4d29a40");
                    },
                    onFail: function (err) {
                    }
                });
            });
        });

    </script>

</body>
</html>