<?php
namespace Home\Controller;

/**
 * 首页控制器
 * Class IndexController
 * @package Home\Controller
 */

class IndexController extends ComController
{

    public function index(){
        $JsConfig=isvConfig();
        $this->assign("_config",$JsConfig);
        $this->display();
    }
}