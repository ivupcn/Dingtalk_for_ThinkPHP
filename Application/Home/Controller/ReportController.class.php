<?php
/**
 * Created by PhpStorm.
 * User: Mak
 * Date: 2016/6/24 0024
 * Time: 17:39
 */

namespace Home\Controller;

class ReportController extends ComController{
    public function index(){
        $report=M("report");
        $day_where[C("DB_PREFIX")."report.report_class"]=1;
        $day_list=$report->where($day_where)->join(C("DB_PREFIX")."report_day ON ".C("DB_PREFIX")."report_day.id=".C("DB_PREFIX")."report.time_id")->select();

        $week_where[C("DB_PREFIX")."report.report_class"]=2;
        $week_list=$report->where($week_where)->join(C("DB_PREFIX")."report_week ON ".C("DB_PREFIX")."report_week.id=".C("DB_PREFIX")."report.time_id")->select();

        $month_where[C("DB_PREFIX")."report.report_class"]=3;
        $month_list=$report->where($month_where)->join(C("DB_PREFIX")."report_month ON ".C("DB_PREFIX")."report_month.id=".C("DB_PREFIX")."report.time_id")->select();

        $tmplist=array_merge($day_list,$week_list);
        $list=array_merge($tmplist,$month_list);
        $this->assign("list",$list);
        $this->display();
    }

    public function day(){
        $report=M("report");
        $where[C("DB_PREFIX")."report.report_class"]=1;
        $list=$report->where($where)->join(C("DB_PREFIX")."report_day ON ".C("DB_PREFIX")."report_day.id=".C("DB_PREFIX")."report.time_id")->select();
        $this->assign("list",$list);
        $this->display();
    }

    public function week(){
        $report=M("report");
        $where[C("DB_PREFIX")."report.report_class"]=2;
        $list=$report->where($where)->join(C("DB_PREFIX")."report_week ON ".C("DB_PREFIX")."report_week.id=".C("DB_PREFIX")."report.time_id")->select();
        $this->assign("list",$list);
        $this->display();
    }

    public function month(){
        $report=M("report");
        $where[C("DB_PREFIX")."report.report_class"]=3;
        $list=$report->where($where)->join(C("DB_PREFIX")."report_month ON ".C("DB_PREFIX")."report_month.id=".C("DB_PREFIX")."report.time_id")->select();
        $this->assign("list",$list);
        $this->display();
    }

    public function add(){
        $time_id=0;
        $report_class=I("post.report_class");
        $unionid = session("unionid");
        switch ($report_class){
            case 1:
                $report_day=M("report_day");
                $date['date']=I("post.date");
                $time_id=$report_day->add($date);
                if (!$time_id){
                    $this->error("添加失败");
                }
                break;

            case 2:
                $report_week=M("report_week");
                $date['year']=I("post.year");
                $date['month']=I("post.month");
                $date['week']=I("post.week");
                $time_id=$report_week->add($date);
                if (!$time_id){
                    $this->error("添加失败");
                }
                break;

            case 3:
                $report_month=M("report_month");
                $date['year']=I("post.year");
                $date['month']=I("post.month");
                $time_id=$report_month->add($date);
                if (!$time_id){
                    $this->error("添加失败");
                }
                break;
        }

        $data["content"]=I("post.content");
        $data["unionid"]=$unionid;
        $data["report_class"]=$report_class;
        $data["time_id"]=$time_id;
        $data["addtime"]=time();
        $report=M("report");
        if ($report->add($data)){
            $this->success("添加成功");
        }else{
            $this->error("添加失败");
        }
    }

    public function addDay(){
        $this->display();
    }

    public function addWeek(){
        $year=array(
            date("Y",time()),
            date("Y",time())-1,
            date("Y",time())-2,
            date("Y",time())-3,
        );
        $this->assign("year",$year);
        $this->display();
    }

    public function addMonth(){
        $year=array(
            date("Y",time()),
            date("Y",time())-1,
            date("Y",time())-2,
            date("Y",time())-3,
        );
        $this->assign("year",$year);
        $this->display();
    }

    public function me(){
        $unionid = session("unionid");
        $report=M("report");
        $day_where[C("DB_PREFIX")."report.report_class"]=1;
        $day_where[C("DB_PREFIX")."report.unionid"]=$unionid;
        $day_list=$report->where($day_where)->join(C("DB_PREFIX")."report_day ON ".C("DB_PREFIX")."report_day.id=".C("DB_PREFIX")."report.time_id")->select();

        $week_where[C("DB_PREFIX")."report.report_class"]=2;
        $week_where[C("DB_PREFIX")."report.unionid"]=$unionid;
        $week_list=$report->where($week_where)->join(C("DB_PREFIX")."report_week ON ".C("DB_PREFIX")."report_week.id=".C("DB_PREFIX")."report.time_id")->select();

        $month_where[C("DB_PREFIX")."report.report_class"]=3;
        $month_where[C("DB_PREFIX")."report.unionid"]=$unionid;
        $month_list=$report->where($month_where)->join(C("DB_PREFIX")."report_month ON ".C("DB_PREFIX")."report_month.id=".C("DB_PREFIX")."report.time_id")->select();

        $tmplist=array_merge($day_list,$week_list);
        $list=array_merge($tmplist,$month_list);
        $this->assign("list",$list);
        $this->display();
    }

    public function others(){
        $unionid = session("unionid");
        $report=M("report");
        $day_where[C("DB_PREFIX")."report.report_class"]=1;
        $day_where[C("DB_PREFIX")."report.unionid"]=array("neq",$unionid);
        $day_list=$report->where($day_where)->join(C("DB_PREFIX")."report_day ON ".C("DB_PREFIX")."report_day.id=".C("DB_PREFIX")."report.time_id")->select();

        $week_where[C("DB_PREFIX")."report.report_class"]=2;
        $week_where[C("DB_PREFIX")."report.unionid"]=array("neq",$unionid);
        $week_list=$report->where($week_where)->join(C("DB_PREFIX")."report_week ON ".C("DB_PREFIX")."report_week.id=".C("DB_PREFIX")."report.time_id")->select();

        $month_where[C("DB_PREFIX")."report.report_class"]=3;
        $month_where[C("DB_PREFIX")."report.unionid"]=array("neq",$unionid);
        $month_list=$report->where($month_where)->join(C("DB_PREFIX")."report_month ON ".C("DB_PREFIX")."report_month.id=".C("DB_PREFIX")."report.time_id")->select();

        $tmplist=array_merge($day_list,$week_list);
        $list=array_merge($tmplist,$month_list);
        $this->assign("list",$list);
        $this->display();
    }
}